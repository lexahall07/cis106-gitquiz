<h1>Bryana Ervin</h1>
<p>I'm a Cuesta College student studying to get my A.S. and Computer Science degree. I love Animal Crossing and Watermelon and my one motto in life is,
"There can never be too many mashed potatoes."</p>
<p>I am very fond of pizza. Pizza is love, pizza is life. My favorite type of pizza is a Veggie Pizza with extra artichoke, olives, tomatoes,
and white sauce.</p>
<p>My favorite place to vacation would either be Oregon or the Philippines. Oregon is beautiful and overcast-y while the Philippines is exotic, 
sunny, and warm.</p>
